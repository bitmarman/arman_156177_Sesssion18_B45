<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 2:11 PM
 */

namespace App;


trait Hello {
    public function sayHello(){
        echo 'Hello ';
    }
}
trait World{
    public function sayWorld(){
        echo 'World';
    }
}
 class Test{
     use Hello, World;
     public function  sayExclamationMark(){
         echo '!';
     }
 }
