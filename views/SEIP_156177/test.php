<?php
require_once("../../vendor/autoload.php");

$obj = new\App\Test();

$obj->sayHello();
$obj->sayWorld();
$obj->sayExclamationMark();

$objAbstract = new \App\\InheritedFromAbstractClass();
$objAbstract->setMyValue("Hello Abstract Class!<br>");
echo $objAbstract->getMyValue();

$objDuck = new \App\Duck();
ec